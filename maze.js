var maze = new Array(20);
var height = 20;
var width = 20;
var canvas_id = "canvas";

function Cell(x, y) {
    this.xpos = x;
    this.ypos = y;
    this.NORTHWALL = true;
    this.SOUTHWALL = true;
    this.EASTWALL = true;
    this.WESTWALL = true;
    this.visited = false;
}


function generateMaze(param_canvas_id) {
    if (param_canvas_id) {
        canvas_id = param_canvas_id;
    }
    initMaze(width, height);

    drawMaze();

    recursiveBacktrack();
}

function initMaze() {
    for (var row = 0; row < 20; row++) {
        maze[row] = new Array(20);
        for (var col = 0; col < 20; col++) {
            maze[row][col] = new Cell(row, col);
        }
    }

}

function recursiveBacktrack() {
    /*
     The depth-first search algorithm of maze generation is frequently implemented using backtracking:

     Make the initial cell the current cell and mark it as visited
     While there are unvisited cells
     If the current cell has any neighbours which have not been visited
     Choose randomly one of the unvisited neighbours
     Push the current cell to the stack
     Remove the wall between the current cell and the chosen cell
     Make the chosen cell the current cell and mark it as visited
     Else if stack is not empty
     Pop a cell from the stack
     Make it the current cell
     Else
     Pick a random cell, make it the current cell and mark it as visited
     End While
     */
    var currentCell = maze[0][0];
    currentCell.visited = true;
    var path = [];
    path.push(currentCell);
    var visited = 1;
    var numCells = maze.length * maze[0].length;


    while (visited != numCells) {
        if (!isAllNeighboursVisited(currentCell)) {
            var randomNeighbour = getRandomNeighbour(currentCell);
            path.push(currentCell);
            removeWall(currentCell, randomNeighbour);
            randomNeighbour.visited = true;
            visited++;
            currentCell = randomNeighbour;

        }
        else if (typeof path !== 'undefined' && path.length > 0) {
            currentCell = path.pop();
        }
        else {
            var random = getRandomCell();
            if (!random.visited) {
                random.visited = true;
                visited++;
            }
            currentCell = random;
            path.push(currentCell);
        }
    }
}

function removeWall(cellFrom, cellTo) {
    var x1 = cellFrom.xpos * 20;
    var y1 = cellFrom.ypos * 20;
    var x2 = cellTo.xpos * 20;
    var y2 = cellTo.ypos * 20;

    var canvas = document.getElementById(canvas_id);
    if (canvas.getContext) {
        var ctx = canvas.getContext("2d");
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#000000';

        if ((x1 - x2) == 0) {
            //then not a connecting path to left or right neighbour cell
            if ((y1 - y2) < 0 && cellFrom.NORTHWALL == true && cellTo.SOUTHWALL == true) {
                //then remove north neighbour wall of cellFrom, and south neighbour wall of cellTo
                ctx.beginPath();
                ctx.moveTo(x2, y2);
                ctx.lineTo(x2 + 20, y2);
                ctx.stroke();
                ctx.closePath();
                cellFrom.NORTHWALL = false;
                cellTo.SOUTHWALL = false;
            }
            else if ((y1 - y2) > 0 && cellFrom.SOUTHWALL == true && cellTo.NORTHWALL == true) {
                //then remove south neighbour wall of cellFrom, and north neighbour wall of cellTo
                ctx.beginPath();
                ctx.moveTo(x2, y2 + 20);
                ctx.lineTo(x2 + 20, y2 + 20);
                ctx.stroke();
                ctx.closePath();
                cellFrom.SOUTHWALL = false;
                cellTo.NORTHWALL = false;
            }
        }
        else if ((y1 - y2) == 0) {
            //then not a connecting path to top or bottom cells
            if ((x1 - x2) < 0 && cellFrom.WESTWALL == true && cellTo.EASTWALL == true) {
                //then remove west neighbour wall of cellFrom, and east neighbour wall of cellTo
                ctx.beginPath();
                ctx.moveTo(x2, y2);
                ctx.lineTo(x2, y2 + 20);
                ctx.stroke();
                ctx.closePath();
                cellFrom.WESTWALL = false;
                cellTo.EASTWALL = false;
            }
            else if ((x1 - x2) > 0 && cellFrom.EASTWALL == true && cellTo.WESTWALL == true) {
                //then remove east neighbour wall of cellFrom, and west neighbour wall of cellTo
                ctx.beginPath();
                ctx.moveTo(x2 + 20, y2);
                ctx.lineTo(x2 + 20, y2 + 20);
                ctx.stroke();
                ctx.closePath();
                cellFrom.EASTWALL = false;
                cellTo.WESTWALL = false;
            }
        }
    }


}

function getAllUnvisitedNeighbours(cell) {
    x = cell.xpos;
    y = cell.ypos;
    var neighbours = [];
    //north
    if (maze[x] != undefined && maze[x][y - 1] != undefined && !maze[x][y - 1].visited) {
        neighbours.push(maze[x][y-1]);
    }
    //south
    if (maze[x] != undefined && maze[x][y + 1] != undefined && !maze[x][y + 1].visited) {
        neighbours.push(maze[x][y+1]);
    }
    //west
    if (maze[x - 1] != undefined && maze[x - 1][y] != undefined && !maze[x - 1][y].visited) {
        neighbours.push(maze[x-1][y]);
    }
    //east
    if (maze[x + 1] != undefined && maze[x + 1][y] != undefined && !maze[x + 1][y].visited) {
        neighbours.push(maze[x+1][y]);
    }
    return neighbours;
}

function isAllNeighboursVisited(cell) {
    //returns true is all neighbours have been visited
    var neighbours = getAllUnvisitedNeighbours(cell);
    for(neighbour in neighbours) {
        if (!neighbours[neighbour].visited) {
            return false;
        }
    }
    return true;
}

function getRandomCell() {
    return maze[getRandomInt(0, maze.length - 1)][getRandomInt(0, maze[0].length - 1)];
}

function getRandomNeighbour(cell) {
    var neighbours = getAllUnvisitedNeighbours(cell);
    return neighbours[getRandomInt(0, neighbours.length - 1)];
}

function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


function drawMaze() {
    canvas = document.getElementById(canvas_id);
    if (canvas.getContext) {
        ctx = canvas.getContext("2d");
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#444444';

        for (var row = 0; row <= maze.length; row++) {
            ctx.beginPath();
            ctx.moveTo(0, row * 20);
            ctx.lineTo(20 * (maze.length), row * 20);
            ctx.stroke();
            ctx.closePath();
        }
        for (var col = 0; col <= maze[0].length; col++) {
            ctx.beginPath();
            ctx.moveTo(col * 20, 0);
            ctx.lineTo(col * 20, 20 * (maze[0].length));
            ctx.stroke();
            ctx.closePath();
        }
    }
    else {
        alert("Could not draw maze, not canvas id found with id = " + canvas_id);
    }
}


